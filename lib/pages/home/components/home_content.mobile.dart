import 'package:flutter/material.dart';
import 'package:flutter_test_index/constants/asset_consts.dart';
import 'package:flutter_test_index/constants/color_consts.dart';
import 'package:flutter_test_index/constants/dimension_consts.dart';
import 'package:flutter_test_index/pages/home/components/home_tab_section.dart';
import 'package:flutter_test_index/utils/image_utils.dart';

class HomeContentMobile extends StatelessWidget {
  const HomeContentMobile({super.key});

  @override
  Widget build(BuildContext context) {
    final headerSection = Container(
      width: kdScreenWidth(context),
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: kcBackgroundGradient,
        ),
      ),
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text('Deine Job Website', style: TextStyle(fontSize: 28)),
          const SizedBox(
            height: 20,
          ),
          const SizedBox(
            width: 40,
          ),
          ImageUtils.getSvgFromAsset(
            kaAggreement,
            fit: BoxFit.fitWidth,
          ),
        ],
      ),
    );

    return Column(
      children: [
        headerSection,
        const SizedBox(
          height: 40,
        ),
        HomeTabSection(),
        Expanded(
          child: PageView(children: <Widget>[
            Text('Page 1'),
            Text('Page 2'),
            Text('Page 3'),
          ]),
        )
      ],
    );
  }
}
