import 'package:flutter/material.dart';
import 'package:flutter_test_index/constants/asset_consts.dart';
import 'package:flutter_test_index/constants/color_consts.dart';
import 'package:flutter_test_index/constants/dimension_consts.dart';
import 'package:flutter_test_index/shared/buttons/custom_gradient_button.dart';
import 'package:flutter_test_index/utils/image_utils.dart';

import 'home_tab_section.dart';
import 'tabPages/page1.dart';
import 'tabPages/page2.dart';
import 'tabPages/page3.dart';

class HomeContentDesktop extends StatelessWidget {
  const HomeContentDesktop({super.key});

  @override
  Widget build(BuildContext context) {
    final headerSection = Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: kcBackgroundGradient,
        ),
      ),
      padding: const EdgeInsets.symmetric(vertical: 40, horizontal: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Spacer(),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text('Deine Job Website', style: TextStyle(fontSize: 28)),
              const SizedBox(
                height: 20,
              ),
              CustomGradientButton(
                onPressed: () {},
                title: 'Kostenlos Registrieren',
              ),
            ],
          ),
          const SizedBox(
            width: 40,
          ),
          Expanded(
            child: CircleAvatar(
              backgroundColor: kcWhite,
              minRadius: 200,
              child: SizedBox(
                width: 300,
                height: 200,
                child: ImageUtils.getSvgFromAsset(
                  kaAggreement,
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
          ),
          Spacer()
        ],
      ),
    );

    return Column(
      children: [
        headerSection,
        const SizedBox(
          height: 40,
        ),
        const HomeTabSection(),
        // PageView(
        //   physics: const NeverScrollableScrollPhysics(),
        //   children: const [
        //     Page1(),
        //     Page2(),
        //     Page3(),
        //   ],
        // ),
      ],
    );
  }
}
