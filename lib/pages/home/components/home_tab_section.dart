import 'package:flutter/material.dart';
import 'package:flutter_test_index/constants/color_consts.dart';
import 'package:flutter_test_index/pages/home/components/tabPages/page1.dart';
import 'package:flutter_test_index/pages/home/components/tabPages/page2.dart';
import 'package:flutter_test_index/pages/home/components/tabPages/page3.dart';

final List<String> tabList = ["Arbeitnehmer", "Arbeitgeber", "Temporärbüro"];

class HomeTabSection extends StatefulWidget {
  const HomeTabSection({super.key});

  @override
  State<HomeTabSection> createState() => _HomeTabSectionState();
}

class _HomeTabSectionState extends State<HomeTabSection>
    with SingleTickerProviderStateMixin {
  TabController? _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: tabList.length);
  }

  @override
  Widget build(BuildContext context) {
    return TabBar(
      controller: _tabController,
      tabs: [...tabList.map((value) => Tab(text: value)).toList()],
      indicatorColor: kcPrimary,
      indicatorSize: TabBarIndicatorSize.tab,
      indicatorWeight: 4,
      labelColor: Colors.black,
      unselectedLabelColor: Colors.grey,
      isScrollable: true,
      onTap: (index) => {},
    );
  }

  @override
  void dispose() {
    if (_tabController != null) {
      _tabController!.dispose();
    }
    super.dispose();
  }
}
