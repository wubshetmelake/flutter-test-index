import 'package:flutter/material.dart';
import 'package:flutter_test_index/constants/color_consts.dart';
import 'package:flutter_test_index/constants/dimension_consts.dart';
import 'package:flutter_test_index/pages/home/components/home_content.desktop.dart';
import 'package:flutter_test_index/pages/home/components/home_content.mobile.dart';
import 'package:flutter_test_index/shared/buttons/custom_gradient_button.dart';
import 'package:flutter_test_index/shared/layouts/main_navbar.dart';
import 'package:flutter_test_index/shared/widgets/custom_appbar.dart';
import 'package:responsive_builder/responsive_builder.dart';

class HomeView extends StatelessWidget {
  const HomeView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        height: 67,
        child: const MainNavbar(),
      ),
      bottomNavigationBar: ScreenTypeLayout.builder(
        mobile: (BuildContext context) => _buildBottomNavButton(context),
        desktop: (BuildContext context) => SizedBox.shrink(),
        tablet: (BuildContext context) => _buildBottomNavButton(context),
      ),
      body: ScreenTypeLayout.builder(
        mobile: (BuildContext context) => const HomeContentMobile(),
        desktop: (BuildContext context) => const HomeContentDesktop(),
        tablet: (BuildContext context) => const HomeContentMobile(),
      ),
    );
  }

  _buildBottomNavButton(BuildContext context) {
    return Container(
      height: 80,
      decoration: const BoxDecoration(
        color: kcWhite,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(25),
          topRight: Radius.circular(25),
        ),
      ),
      child: Center(
        child: CustomGradientButton(
          width: kdScreenWidth(context) * .80,
          onPressed: () {},
          title: 'Kostenlos Registrieren',
        ),
      ),
    );
  }
}
