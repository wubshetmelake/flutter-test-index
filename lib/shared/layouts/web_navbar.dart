import 'package:flutter/material.dart';
import 'package:flutter_test_index/constants/color_consts.dart';
import 'package:flutter_test_index/shared/buttons/custom_outlined_button.dart';
import 'package:flutter_test_index/shared/widgets/custom_appbar.dart';

class WebNavbar extends StatelessWidget {
  const WebNavbar({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        const Text(
          'Jetzt Klicken',
          style: TextStyle(fontSize: 19),
        ),
        SizedBox(
          width: 30,
        ),
        CustomOutlinedButton(
          onPressed: () {},
          child: const Text(
            'Kostenlos Registrieren',
            style: TextStyle(color: kcPrimary, fontSize: 18),
          ),
          elevation: 0,
        ),
        SizedBox(
          width: 20,
        ),
        TextButton(
          onPressed: () => {},
          child: TextButton(
            onPressed: () {},
            child: const Text(
              'Login',
              style: TextStyle(color: kcPrimary, fontSize: 20),
            ),
          ),
        )
      ],
    );
  }
}
