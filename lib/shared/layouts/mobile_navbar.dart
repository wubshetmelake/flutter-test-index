import 'package:flutter/material.dart';
import 'package:flutter_test_index/constants/color_consts.dart';

class MobileNavbar extends StatelessWidget {
  const MobileNavbar({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        TextButton(
          onPressed: () => {},
          child: TextButton(
            onPressed: () {},
            child: const Text(
              'Login',
              style: TextStyle(color: kcPrimary, fontSize: 14),
            ),
          ),
        )
      ],
    );
  }
}
