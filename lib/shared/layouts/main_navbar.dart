import 'package:flutter/material.dart';
import 'package:flutter_test_index/shared/layouts/mobile_navbar.dart';
import 'package:flutter_test_index/shared/layouts/web_navbar.dart';
import 'package:responsive_builder/responsive_builder.dart';

class MainNavbar extends StatelessWidget {
  const MainNavbar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
      mobile: (BuildContext context) => const MobileNavbar(),
      desktop: (BuildContext context) => const WebNavbar(),
    );
  }
}
