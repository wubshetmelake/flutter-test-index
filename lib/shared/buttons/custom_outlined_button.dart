import 'package:flutter/material.dart';
import 'package:flutter_test_index/constants/dimension_consts.dart';

class CustomOutlinedButton extends StatelessWidget {
  final Color color;
  final Function onPressed;
  final Widget child;
  final double radius;
  final double width;
  final double height;
  final double elevation;

  const CustomOutlinedButton({
    Key? key,
    required this.onPressed,
    required this.child,
    this.color = Colors.white,
    this.radius = 18,
    this.width = 200,
    this.height = kdButtonHeight,
    this.elevation = 6.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      height: height,
      minWidth: width,
      elevation: elevation,
      color: color,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(radius),
          side: const BorderSide(color: Colors.black12)),
      onPressed: onPressed as void Function()?,
      child: child,
    );
  }
}
