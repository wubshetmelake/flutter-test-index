import 'package:flutter/material.dart';
import 'package:flutter_test_index/constants/color_consts.dart';
import 'package:flutter_test_index/constants/dimension_consts.dart';

class CustomGradientButton extends StatelessWidget {
  final double? width;
  final double height;
  final String title;
  final Function onPressed;
  final bool isDisabled;
  final double radius;
  final List<Color> colors;
  final Color textColor;

  const CustomGradientButton(
      {Key? key,
      this.width = 250,
      this.height = kdButtonHeight,
      required this.title,
      required this.onPressed,
      this.isDisabled = false,
      this.radius = kdRadiusButton,
      this.textColor = kcWhite,
      this.colors = kcPrimaryGradient})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final content = Container(
    //   child: Center(
    //       child: Text(
    //     title,
    //     style: buttonStyle.copyWith(color: kcWhite),
    //   )),
    // );

    final content = Center(
        child: Text(
      title,
      style: TextStyle(color: textColor, fontSize: 14),
    ));

    return GestureDetector(
      onTap: onPressed as void Function()?,
      child: Container(
        width: width,
        height: height,
        child: content,
        decoration: BoxDecoration(
          gradient: isDisabled
              ? null
              : LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: colors,
                ),
          borderRadius: BorderRadius.circular(radius),
          // boxShadow: [
          //   BoxShadow(
          //     color: kcPrimary.withAlpha(200),
          //     blurRadius: 5.0,
          //     spreadRadius: 3.0,
          //     offset: const Offset(
          //       0.0,
          //       5.0,
          //     ),
          //   ),
          // ],
          color: isDisabled ? Colors.grey.shade400 : null,
        ),
      ),
    );
  }
}
