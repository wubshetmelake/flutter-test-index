import 'package:flutter/material.dart';
import 'package:flutter_test_index/constants/color_consts.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final Widget child;
  final double height;

  final double topBorderHight = 6;

  CustomAppBar({
    required this.child,
    this.height = kToolbarHeight,
  });

  @override
  Size get preferredSize => Size.fromHeight(height);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          height: topBorderHight,
          decoration: const BoxDecoration(
              gradient: LinearGradient(colors: kcPrimaryGradient)),
          alignment: Alignment.center,
        ),
        Container(
          height: preferredSize.height - 10,
          decoration: BoxDecoration(
            color: kcWhite,
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25),
            ),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                spreadRadius: 3,
                blurRadius: 7,
                offset: Offset(0, 3), // Shadow position
              ),
            ],
          ),
          alignment: Alignment.center,
          child: child,
        ),
      ],
    );
  }
}
