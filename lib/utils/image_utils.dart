import 'package:flutter/material.dart';
import 'package:flutter_test_index/constants/asset_consts.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ImageUtils {
  ImageUtils._();
  static Widget getAssetImage(String? src,
      {double? height, double? width, fit = BoxFit.cover}) {
    if (src == null || src.isEmpty) {
      return Image.asset(
        kaAggreement,
        height: height,
        width: width,
        fit: fit,
      );
    }
    return Image.asset(
      src,
      height: height,
      width: width,
      fit: fit,
    );
  }

  static Widget getSvgFromAsset(String src,
      {double? height, double? width, fit = BoxFit.cover}) {
    return SvgPicture.asset(
      src,
      width: width,
      height: height,
      fit: fit,
    );
  }
}
