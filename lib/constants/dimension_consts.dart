import 'package:flutter/material.dart';

const double kdButtonHeight = 40;

const double kdRadius = 10;
const double kdRadiusContainer = 30;
const double kdRadiusButton = 60;

double kdScreenWidth(BuildContext context) {
  return MediaQuery.of(context).size.width;
}

double kdScreenHeight(BuildContext context) {
  return MediaQuery.of(context).size.height;
}
