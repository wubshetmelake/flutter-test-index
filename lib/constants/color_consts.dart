// App Colors
import 'package:flutter/material.dart';

const kcPrimary = Color(0xFF319795);
const kcSecondary = Color(0xFF3182CE);
const kcBackground = Color(0xFFEBF4FF);
const kcBackground_variant = Color(0xFFE6FFFA);
const kcWhite = Color(0xFFFFFFFF);

// Gradients
const kcPrimaryGradient = [Color(0xFF319795), Color(0xFF3182CE)];
const kcBackgroundGradient = [Color(0xFFEBF4FF), Color(0xFFE6FFFA)];

// Supportive Colors
const kcErrorColor = Color(0xFFb00020);
const kcDarkErrorColor = Color(0xFFCF6679);
const kcGreen = Color(0xFF319A64);
const kcGold = Color(0xFFFFCC00);
const kcOrange = Color(0xFFEC9E2A);
